from django.urls import path
from .views import LoginView, LogoutView, signup


urlpatterns = [
    path("login/", LoginView, name="login"),
    path("logout/", LogoutView, name="logout"),
    path("signup/", signup, name="signup"),
]
