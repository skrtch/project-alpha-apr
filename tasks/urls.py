from django.urls import path
from .views import CreateTask, ListTask

urlpatterns = [
    path("create/", CreateTask, name="create_task"),
    path("mine/", ListTask, name="show_my_tasks"),
]
