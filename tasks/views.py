from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from .models import Task


# Create your views here.
@login_required
def CreateTask(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
        context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def ListTask(request):
    tasks = Task.objects.all()
    context = {"tasks": tasks}
    return render(request, "tasks/list_tasks.html", context)
